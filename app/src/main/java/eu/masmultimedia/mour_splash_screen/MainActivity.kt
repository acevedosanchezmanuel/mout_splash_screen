package eu.masmultimedia.mour_splash_screen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.window.SplashScreen

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        setTheme(R.style.Theme_Mour_splash_screen)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}